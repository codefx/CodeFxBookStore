﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Interfaces;
using CodeFx.Infrastructure.Structs;
using System;
using CodeFx.ViewModel.Account;
using CodeFx.UI.Core.Account;
using CodeFx.UI.Core.Manager;
using System.Linq;
using CodeFx.Infrastructure.Security;
using CodeFx.UI.Core.Common;

namespace CodeFx.Business.Implements
{
    public class AccountBusiness : IAccountBusiness
    {

        /*
                Member Register -> herkese acik. ayni e-mail adresi ikinci kez olamaz.
                User Register -> Console ekraninda login olmus ve yetkisi olan kullanici tarafindan yapilabilir
                bunlar bitecek

                front end side -> angular || react -> 
             */
        private readonly ISessionManager _sessionManager;
        private readonly IUserRepository _userRepository;
        private readonly IMemberRepository _memberRepository;
        private readonly IHashProvider _hashProvider;

        public AccountBusiness(ISessionManager sessionManager, IUserRepository userRepository, IMemberRepository memberRepository, IHashProvider hashProvider)
        {
            _sessionManager = sessionManager;
            _userRepository = userRepository;
            _memberRepository = memberRepository;
            _hashProvider = hashProvider;
        }

        public ResultSet<Login> LoginUser(Login model)
        {
            var result = new ResultSet<Login>
            {
                Object = model
            };

            var checkModel = CheckLoginModel(model);

            if (!checkModel.Success)
            {
                result.Message = checkModel.Message;
                return result;
            }

            // Method ismine dikkat USER
            var userCheck = _userRepository.GetUserByEmail(model.Email);

            if (userCheck != null)
            {
                // sifresini kontrol edecegiz. 

                //PasswordHash = combine(model.Password, userCheck.PasswordSalt);

                var isAuthenticated = _hashProvider.VerifyHashString(model.Password, userCheck.PasswordHash, userCheck.PasswordSalt);

                //string passHash, passSalt;

                //_hashProvider.GetHashAndSaltString(model.Password, out passHash, out passSalt);

                //memberCheck.PasswordSalt = passSalt;
                //memberCheck.PasswordHash = passHash;

                if (isAuthenticated)
                {
                    // haklarini alacagiz. 1- once kendi kisiel haklarini, 2- sonra rol haklarini.

                    var rolePermission = _userRepository.GetUserRolePermission(userCheck);
                    var permit = userCheck.UserPermissions.Where(up => up.IsAccessible).Select(x => x.Permission.Key);
                    var unPermit = userCheck.UserPermissions.Where(x => !x.IsAccessible).Select(x => x.Permission.Key);

                    AccountInfo accountInfo = new AccountInfo
                    {
                        Id = userCheck.Id,
                        FullName = string.Format("{0} {1}", userCheck.Name, userCheck.Surname),
                        Email = model.Email,
                        LoginStatus = true,
                        Status = Infrastructure.Enums.Status.Active,
                        Username = model.Email,
                        ParentId = userCheck.ParentUserId != null ? userCheck.ParentUserId.Value : 0
                    };
                    
                    // role de yetkisi olup adamin kisisel yetkisi yasakli ise rol de olsa bile izin vermiyoruz. UserPermission tablosundaki IsAccessible alaninin FALSE olmasi ile anliyoruz
                    //burasi cok onemli. ***
                    accountInfo.Rights = rolePermission.Where(p => !unPermit.Contains(p.Key)).Select(p => p.Key).ToList();

                    //// kisisel yetkilerinde kullanilmasinia izin verilenleri IsAccessible alaninin TRUE olmasi ile anliyoruz. Burasi cok onemli ****
                    accountInfo.Rights.AddRange(permit);

                    _sessionManager.Set(Keys.Session.CurrentAccountInfo, accountInfo);

                    result.Success = true;
                }
                else
                {
                    result.Message = "Kullanıcı adı veya şifre hatalı.";
                }
            }
            else
            {
                result.Message = "Kullanıcı adı veya şifre hatalı.";
            }
            return result;
        }

        public ResultSet<Login> LoginMember(Login model)
        {
            var result = new ResultSet<Login>
            {
                Object = model
            };

            var checkModel = CheckLoginModel(model);

            if (!checkModel.Success)
            {
                result.Message = checkModel.Message;
                return result;
            }

            // Method ismine dikkat Member
            var memberCheck = _memberRepository.GetMemberByEmail(model.Email);

            if (memberCheck != null)
            {
                // sifresini kontrol edecegiz. 

                var isAuthenticated = _hashProvider.VerifyHashString(model.Password, memberCheck.PasswordHash, memberCheck.PasswordSalt);

                //string passHash, passSalt;
                //_hashProvider.GetHashAndSaltString(model.Password, out passHash, out passSalt);
                //memberCheck.PasswordSalt = passSalt;
                //memberCheck.PasswordHash = passHash;


                if (isAuthenticated)
                {
                    // haklarini alacagiz. 1- once kendi kisiel haklarini, 2- sonra rol haklarini.

                    var rolePermission = _memberRepository.GetMemberRolePermission(memberCheck);//genel yetkiler

                    var permit = memberCheck.MemberPermissions.Where(up => up.IsAccessible).Select(x => x.Permission.Key);//kisisel kısıt olmayan

                    //kodu tekrar kontrol et?
                    var unPermit = memberCheck.MemberPermissions.Where(x => !x.IsAccessible).Select(x => x.Permission.Key); // kisisel kısıtlı olanlar

                    AccountInfo accountInfo = new AccountInfo
                    {
                        Id = memberCheck.Id,
                        FullName = string.Format("{0} {1}", memberCheck.Name, memberCheck.Surname),
                        Email = model.Email,
                        LoginStatus = true,
                        Status = Infrastructure.Enums.Status.Active,
                        Username = model.Email,

                        // role de yetkisi olup adamin kisisel yetkisi yasakli ise rol de olsa bile izin vermiyoruz. UserPErmission tablosundaki IsAccessible alaninin FALSE olmasi ile anliyoruz
                        //burasi cok onemli. ***
                        Rights = rolePermission.Where(p => !unPermit.Contains(p.Key)).Select(p => p.Key).ToList()
                    };

                    // kisisel yetkilerinde kullanilmasinia izin verilenleri IsAccessible alaninin TRUE olmasi ile anliyoruz. Burasi cok onemli ****
                    accountInfo.Rights.AddRange(permit);

                    // Session isimlerinin ayni olmasinin bir sıkıntısı olmazç Cunku farklı Projeler olarak calısacaklar.
                    _sessionManager.Set(Keys.Session.CurrentAccountInfo, accountInfo);
                    
                    result.Success = true;
                }
                else
                {
                    result.Message = "Kullanıcı adı veya şifre hatalı.";
                }
            }
            else
            {
                result.Message = "Kullanıcı adı veya şifre hatalı.";
            }

            return result;
        }
        public void LogoutAccount()
        {
            _sessionManager.Remove(Keys.Session.CurrentAccountInfo);
        }
        
        private ResultSet CheckLoginModel(Login model)
        {
            var result = new ResultSet();
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                result.Message = "Kullanıcı adı yazmalısınız.";
                return result;
            }
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                result.Message = "Şifre yazmalısınız.";
                return result;
            }
            result.SetSuccessful();
            return result;
        }

        private ResultSet CheckRegisterModel(Register model)
        {
            var result = new ResultSet();
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                result.Message = "Kullanıcı adı yazmalısınız.";
                return result;
            }

            //  for member(Customer) -> @ c@c.c
            //  for user(Admin) -> @ c@c it could be


            if (string.IsNullOrWhiteSpace(model.Password))
            {
                result.Message = "Şifre yazmalısınız.";
                return result;
            }

            if (!model.Password.Equals(model.ConfirmPassword))
            {
                result.Message = "Şifre ve Şifre tekrar aynı değil.";
                return result;
            }
            result.SetSuccessful();
            return result;
        }
    }
}
