﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.ViewModel.Author;
using CodeFx.ViewModel.Book;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace CodeFx.Business.Implements
{
    public class AuthorBusiness: IAuthorBusiness
    {
        private IAuthorRepository repo;

        public AuthorBusiness(IAuthorRepository authorRepository)
        {
            repo = authorRepository;
        }
        public IEnumerable<AuthorItem> GetAuthors()
        {
            var list = repo.GetAll().ToList();
            List<AuthorItem> ItemList = new List<AuthorItem>();

            foreach (Author item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }

        public AuthorItem GetById(int id)
        {
            var b = repo.Get(id);
            return ConvertToViewModel(b);
        }

        public AuthorItem ConvertToViewModel(Author a)
        {
            var result = new AuthorItem
            {
                Id = a.Id,
                Name = a.Name,
                Surname= a.Surname
            };
            return result;
        }
        private Author ConvertToDbItem(AuthorItem a)
        {
            var ath = new Author
            {
                Id = a.Id,
                Name = a.Name,
                 Surname=a.Surname
            };
            return ath;
        }
        public bool AddAuthor(AuthorItem a)
        {
            var ath = ConvertToDbItem(a);
            var newAth = repo.Add(ath);
            return true;
        }

        private BookForAuthorItem ConvertToViewModelwithBooks(Book b)
        {
            var result = new BookForAuthorItem
            {
                Id = b.Id,
                Name = b.Name,
                ImageThumbnailUrl = b.ImageThumbnailUrl,
                AuthorId= b.AuthorId
            };
            return result;
        }
        public AuthorItem Update(AuthorItem a)
        {
            var toDbAth = ConvertToDbItem(a);
            var athRet = repo.Update(toDbAth, toDbAth.Id);

            return ConvertToViewModel(toDbAth);
        }
        public AuthorWithBooks ConvertAuthorWithBooksToViewModel(Author a)
        {
            List<BookForAuthorItem> ItemList = new List<BookForAuthorItem>();
            foreach (var item in a.Books)
            {
                ItemList.Add(ConvertToViewModelwithBooks(item));
            }
            var list = new AuthorWithBooks
            {
                AuthorId = a.Id,
                Name=a.Name+" "+a.Surname,
                Books = ItemList
            };

            return list;
        }
        public List<AuthorWithBooks> AuthorsWithBooks(int id)
        {
            var result = new List<AuthorWithBooks>();
            var list = repo.GetAuthorWithBooks().Where(x => x.Id == id);
            foreach (var item in list)
            {
                result.Add(ConvertAuthorWithBooksToViewModel(item));
            }
            return result;
        }
       
        //for console
        public IEnumerable<SelectListItem> GetDropDownAuthors()
        {
            //var x = new (Select(x => new SelectListItem()
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString()
            //}
            return repo.GetAll().Select(x => new SelectListItem()
            {
                Text = x.Name+" "+x.Surname,
                Value = x.Id.ToString()
            });
            //var drp = new DrpDwnCategoryItem() { Selectlist = liste };
            //return drp;
        }
    }
}
