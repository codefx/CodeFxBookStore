﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.Infrastructure.Enums;
using CodeFx.ViewModel.Book;
using CodeFx.ViewModel.Category;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace CodeFx.Business.Implements
{
    public class BookBusiness : IBookBusiness
    {
        private IBookRepository repo;
        private readonly ICategoryBusiness _categoryBusiness;
        private readonly IPublishingHouseBusiness _publishingHouseBusiness;
        private readonly IAuthorBusiness _authorBusiness;

        public BookBusiness(IBookRepository bookRepository,
            ICategoryBusiness categoryBusiness,
            IPublishingHouseBusiness publishingHouseBusiness,
            IAuthorBusiness authorBusiness
            )
        {
            repo = bookRepository;
            _categoryBusiness = categoryBusiness;
            _publishingHouseBusiness = publishingHouseBusiness;
            _authorBusiness = authorBusiness;
        }

        //public IEnumerable<BookItem> GetLatestBooks()
        //{
        //    var list = repo.GetAll().OrderByDescending(x=>x.Id).Take(10).ToList();
        //    List<BookItem> BItemList = new List<BookItem>();

        //    foreach (Book item in list)
        //    {
        //        BItemList.Add(ConvertToViewModel(item));
        //    }
        //    return BItemList;
            
        //}
        public IEnumerable<BookItem> GetBooksSearch(string q)
        {
            var list = repo.GetAll();
            if (!string.IsNullOrEmpty(q))
            {
                q = q.ToLower();
                list = list.Where(x => x.Name.ToLower().Contains(q)||x.ShortDescription.ToLower().Contains(q));
            }
            else
            {
                list=list.OrderByDescending(x => x.Id).Take(10);
            }
            var t = list.ToList();
            List<BookItem> BItemList = new List<BookItem>();

            foreach (Book item in t)
            {
                BItemList.Add(ConvertToViewModel(item));
            }
            return BItemList;

        }

        public IEnumerable<Book> GetBooksConsole()
        {
            var list = repo.GetAll().ToList();
            
            return list;
        }

        private BookItem ConvertToViewModel(Book b)
        {
            // genelde ilk olarak Cache sistemi kontrol ediliyor tip donusumleri yapilir.
            var result = new BookItem
            {
                Id = b.Id,
                Name = b.Name,
                Isbn = b.Isbn,
                Author = b.Author.Name + " " + b.Author.Surname,
                Price = b.Price,
                PublishingHouse = b.PublishingHouse.Name,
                PageCount = b.PageCount,
                Edition = b.Edition,
                Language = b.Language,
                ShortDescription = b.ShortDescription,
                Volume = b.Volume,
                Category = new CategoryItem
                {
                    Id = b.Category.Id,
                    Name = b.Category.Name
                }
            };

            return result;
        }

        public BookItem GetById(int id) {
            // genelde ilk olarak Cache sistemi kontrol ediliyor tip donusumleri yapilir.
            var b = repo.Get(id);

            return ConvertToViewModel(b);
        }

        public BookEdit GetBookEditById(int id) {

            var dbItem = repo.Get(id);

            if (dbItem == null) {
                return null;
            }

            BookEdit obj = new BookEdit {
                Id = dbItem.Id,
                Name = dbItem.Name,
                Isbn = dbItem.Isbn,
                AuthorId = dbItem.AuthorId,
                PublishingHouseId = dbItem.PublishingHouseId,
                Language = dbItem.Language,
                PageCount = dbItem.PageCount,
                Edition = dbItem.Edition,
                Volume = dbItem.Volume,
                ImageUrl = dbItem.ImageUrl,
                ImageThumbnailUrl = dbItem.ImageThumbnailUrl,
                ShortDescription = dbItem.ShortDescription,
                Price = dbItem.Price,
                InStock = dbItem.InStock,
                CategoryId = dbItem.CategoryId != null ? dbItem.CategoryId.Value : 0,
                CategoryList = _categoryBusiness.GetDropDownCategories(),
                AuthorList = _authorBusiness.GetDropDownAuthors(),
                PublishingHouseList = _publishingHouseBusiness.GetDropDownPublishingHouses()
            };

            return obj;
        }

        public BookEdit GetBookEditForCreate()
        {

            BookEdit obj = new BookEdit
            {
                CategoryList = _categoryBusiness.GetDropDownCategories(),
                AuthorList = _authorBusiness.GetDropDownAuthors(),
                PublishingHouseList = _publishingHouseBusiness.GetDropDownPublishingHouses()
            };

            return obj;
        }

        private Book ConvertToDbItem(BookEdit b)
        {
            var book = new Book
            {
                Id = b.Id,
                AuthorId = b.AuthorId,
                CategoryId = b.CategoryId,
                Language = b.Language,
                PageCount = b.PageCount,
                Edition = b.Edition,
                InStock = b.InStock,
                Volume = b.Volume,
                ImageUrl = b.ImageUrl,
                ImageThumbnailUrl = b.ImageThumbnailUrl,
                ShortDescription = b.ShortDescription,
                PublishingHouseId = b.PublishingHouseId,
                Price = b.Price,
                Isbn = b.Isbn,
                Name = b.Name

            };
            return book;
        }

        public BookEdit Update(BookEdit b)
        {
            var toDbBook = ConvertToDbItem(b);
            var bret= repo.Update(toDbBook, toDbBook.Id);

            return GetBookEditById(bret.Id);
        }

        public bool AddBook(BookEdit b)
        {
            var adb = ConvertToDbItem(b);
           var addedBook= repo.Add(adb);
            return true;

        }
    }
}
