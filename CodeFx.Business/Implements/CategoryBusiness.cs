﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.ViewModel.Category;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using CodeFx.ViewModel.Book;

namespace CodeFx.Business.Implements
{
    public class CategoryBusiness:ICategoryBusiness
    {
        private ICategoryRepository repo;

        public CategoryBusiness(ICategoryRepository categoryRepository)
        {
            repo = categoryRepository;
        }
        public IEnumerable<CategoryItem> GetCategories()
        {
            var list = repo.GetAll().ToList();
            List<CategoryItem> ItemList = new List<CategoryItem>();

            foreach (Category item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }
        public CategoryItem GetById(int id)
        {
            // genelde ilk olarak Cache sistemi kontrol ediliyor tip donusumleri yapilir.
            var b = repo.Get(id);

            return ConvertToViewModel(b);
        }

        private CategoryItem ConvertToViewModel(Category b)
        {
            var result = new CategoryItem
            {
                Id = b.Id,
                Name = b.Name,
                Description=b.Description
            };

            return result;
        }
        private Category ConvertToDbItem(CategoryItem c)
        {
            var cat = new Category
            {
                Id = c.Id,
                Name = c.Name,
                Description = c.Description
            };
            return cat;
        }
        public bool AddCategory(CategoryItem c)
        {
            var ctg = ConvertToDbItem(c);
            var newCat = repo.Add(ctg);
            return true;
        }
        public CategoryItem Update(CategoryItem c)
        {
            var toDbCat = ConvertToDbItem(c);
            var cret = repo.Update(toDbCat, toDbCat.Id);
            
            return ConvertToViewModel(cret);
        }

        private BookforCtgItem ConvertToViewModelwithBooks(Book b)
        {
            var result = new BookforCtgItem
            {
                Id=b.Id,
                Name=b.Name,
                 ImageThumbnailUrl=b.ImageThumbnailUrl,
                 CategoryId=b.CategoryId
            };
            return result;            
        }
        public CategoriesWithBooks ConvertCategoriesWithBooksToViewModel(Category c)
        {
            List<BookforCtgItem> ItemList = new List<BookforCtgItem>();
            foreach (var item in c.Books)
            {
                ItemList.Add(ConvertToViewModelwithBooks(item));
            }
            var list = new CategoriesWithBooks
            {
                CategoryId = c.Id,
                Name=c.Name,
                Books = ItemList
            };

            return list;
        }
        public List<CategoriesWithBooks> CategoriesWithBooks(int id)
        {
            var result = new List<CategoriesWithBooks>();
            var list = repo.GetCategorieswithBooks().Where(x=>x.Id==id);
            foreach (var item in list)
            {
                result.Add(ConvertCategoriesWithBooksToViewModel(item));
            }
            return result;
        }



        //ibretlik kod olarak kalsin.
        // hedef bu kodlamayi yapmak/
        public TReturn DynamicConvertToViewModel<TReturn, TInCome>(TInCome t ) where TReturn : class, new() where TInCome : class, new()
        {
            var result = new TReturn();


            //TInCome object reflection part is here.


            return default(TReturn);
        }
        public IEnumerable<SelectListItem> GetDropDownCategories()
        {
            //var x = new (Select(x => new SelectListItem()
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString()
            //}
            return repo.GetAll().Select(x => new SelectListItem()
                                {
                                    Text = x.Name,
                                    Value = x.Id.ToString()                 
                                });
            //var drp = new DrpDwnCategoryItem() { Selectlist = liste };
            //return drp;
        }
    }
}
