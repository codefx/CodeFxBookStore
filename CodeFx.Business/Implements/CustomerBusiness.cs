﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.ViewModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Business.Implements
{
    public class CustomerBusiness : ICustomerBusiness
    {
        private ICustomerRepository repo;
        private readonly IOrderBusiness _orderBusiness;

        public CustomerBusiness(ICustomerRepository customerRepository, IOrderBusiness orderBusiness)
        {
            repo = customerRepository;
            _orderBusiness = orderBusiness;
        }

        public IEnumerable<CustomerItem> GetCustomers()
        {
            var list = repo.GetAll().ToList();
            List<CustomerItem> ItemList = new List<CustomerItem>();

            foreach (Customer item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }

        public CustomerItem GetById(int id)
        {
            var b = repo.Get(id);
            return ConvertToViewModel(b);
        }

        private CustomerItem ConvertToViewModel(Customer c)
        {

            var oItems = _orderBusiness.GetOrders(c.Id);

            var result = new CustomerItem
            {
                Id = c.Id,
                FullName = c.FirstName + " " + c.LastName,
                OrderItems = oItems
            };
            return result;
        }
        private Customer ConvertToDbItem(NewCustomerItem c)
        {
            var result = new Customer
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                AddressLine1 = c.AddressLine1,
                AddressLine2 = c.AddressLine2,
                City = c.City,
                Email = c.Email,
                PhoneNumber = c.PhoneNumber,
                ZipCode = c.ZipCode
            };
            return result;
        }
        private NewCustomerItem ConvertToEditViewModel(Customer c)
        {
            var result = new NewCustomerItem
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                AddressLine1 = c.AddressLine1,
                AddressLine2 = c.AddressLine2,
                City = c.City,
                Email = c.Email,
                PhoneNumber = c.PhoneNumber,
                ZipCode = c.ZipCode
            };
            return result;

        }
        public bool AddNewCustomer(NewCustomerItem c)
        {
            var nc = ConvertToDbItem(c);
            var newCust = repo.Add(nc);
            return true;
        }
        public NewCustomerItem Update(NewCustomerItem c)
        {
            var toDbCust = ConvertToDbItem(c);
            var custRet = repo.Update(toDbCust, toDbCust.Id);

            return ConvertToEditViewModel(custRet);
        }
       

    }
}
