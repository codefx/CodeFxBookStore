﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.ViewModel.Order;
using CodeFx.ViewModel.OrderDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Business.Implements
{
    public class OrderBusiness : IOrderBusiness
    {
        private IOrderRepository repo;
        private IOrderDetailBusiness _orderDetailBusiness;

        public OrderBusiness(IOrderRepository orderRepository, IOrderDetailBusiness orderDetailBusiness)
        {
            repo = orderRepository;
            _orderDetailBusiness = orderDetailBusiness;
        }
        public IEnumerable<OrderItem> GetOrders(int id)
        {
            var list = repo.GetAll().ToList().Where(x=>x.Id==id);
            List<OrderItem> ItemList = new List<OrderItem>();

            foreach (Order item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }
        public IEnumerable<OrderItem> GetAllOrders()
        {
            var list = repo.GetAll().ToList();
            List<OrderItem> ItemList = new List<OrderItem>();

            foreach (Order item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }

        public IEnumerable<OrderDetailItem> GetOrderDetails(int id)
        {
            var b = _orderDetailBusiness.GetOrderDetails(id);
            return b;
        }
        public OrderDetailItem ConvertToViewModel(OrderDetail od)
        {
            var result = new OrderDetailItem
            {
                Id = od.Id,
                BookId = od.BookId,
                BookName=od.Book.Name,
                OrderId = od.OrderId,
                Quantity = od.Quantity,
                UnitPrice = od.UnitPrice,
                Amount = od.Amount
            };
            return result;
        }

        private OrderItem ConvertToViewModel(Order o)
        {
            List<OrderDetailItem> ItemList = new List<OrderDetailItem>();
            var detailList = _orderDetailBusiness.GetOrderDetails(o.Id);

            foreach (var item in o.OrderDetails) //detailList)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            var result = new OrderItem
            {
                Id = o.Id,
                CustomerId=o.CustomerId,
                OrderDate=o.OrderDate,
                OrderDetailItems=ItemList,
                OrderTotal=o.OrderTotal
            };

            return result;
        }
        public OrderDetail ConvertToDbItem(OrderDetailItem odi)
        {
            var result = new OrderDetail
            {
                Id = odi.Id,
                BookId = odi.BookId,
                OrderId = odi.OrderId,
                Quantity = odi.Quantity,
                UnitPrice = odi.UnitPrice
            };

            return result;
        }

        private Order ConvertToDbItem(OrderItem o)
        {
            List<OrderDetail> itemList = new List<OrderDetail>();
            foreach (var item in o.OrderDetailItems)
            {
                itemList.Add(ConvertToDbItem(item));
            }
            var result = new Order
            {
                Id = o.Id,
                CustomerId = o.CustomerId,
                OrderDate = o.OrderDate,
                OrderDetails = itemList,
                OrderTotal = o.OrderTotal
            };

            return result;
        }
        public bool AddOrder(OrderItem oi)
        {
            var o = ConvertToDbItem(oi);
            var newCust = repo.Add(o);
            return true;
        }
    }
}
