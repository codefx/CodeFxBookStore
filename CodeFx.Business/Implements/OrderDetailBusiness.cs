﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.ViewModel.OrderDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Business.Implements
{
    public class OrderDetailBusiness:IOrderDetailBusiness
    {
        
        private IOrderDetailRepository repo;

        public OrderDetailBusiness(IOrderDetailRepository orderDetailRepository)
        {
           
            repo = orderDetailRepository;
        }
        public IEnumerable<OrderDetailItem> GetOrderDetails(int id)
        {
            var list = repo.GetAll().ToList().Where(x => x.Id == id);
            List<OrderDetailItem> ItemList = new List<OrderDetailItem>();

            foreach (OrderDetail item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }
        
        public OrderDetailItem ConvertToViewModel(OrderDetail od)
        {
            var result = new OrderDetailItem
            {
                Id = od.Id,
                BookId = od.BookId,
                 BookName=od.Book.Name,
                OrderId = od.OrderId,
                Quantity = od.Quantity,
                UnitPrice = od.UnitPrice,
                Amount = od.Amount
            };
            return result;
        }
        public OrderDetail ConvertToDbItem(OrderDetailItem odi)
        {
            var result = new OrderDetail
            {
                Id = odi.Id,
                BookId = odi.BookId,
                OrderId = odi.OrderId,
                Quantity = odi.Quantity,
                UnitPrice = odi.UnitPrice
            };

            return result;
        }
        public OrderDetail AddOrderDetail(OrderDetailItem odi)
        {
            var od = ConvertToDbItem(odi);
            return repo.Add(od);
        }
    }
}
