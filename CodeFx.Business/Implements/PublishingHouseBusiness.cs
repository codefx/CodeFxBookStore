﻿using CodeFx.Business.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using CodeFx.ViewModel.Book;
using CodeFx.ViewModel.PublishingHouse;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Business.Implements
{
    public class PublishingHouseBusiness: IPublishingHouseBusiness
    {
        private IPublishingHouseRepository repo;

        public PublishingHouseBusiness(IPublishingHouseRepository publishingHouseRepository)
        {
            repo = publishingHouseRepository;
        }
        public IEnumerable<PublishingHouseItem> GetPublishingHouses()
        {
            var list = repo.GetAll().ToList();
            List<PublishingHouseItem> ItemList = new List<PublishingHouseItem>();

            foreach (PublishingHouse item in list)
            {
                ItemList.Add(ConvertToViewModel(item));
            }
            return ItemList;
        }

        public PublishingHouseItem GetById(int id)
        {
            var b = repo.Get(id);
            return ConvertToViewModel(b);
        }

        public PublishingHouseItem ConvertToViewModel(PublishingHouse p)
        {
            var result = new PublishingHouseItem
            {
                Id = p.Id,
                Name = p.Name,
               Address=p.Address,
               TelNumber=p.TelNumber
            };
            return result;
        }

        private PublishingHouse ConvertToDbItem(PublishingHouseItem p)
        {
            var ph = new PublishingHouse
            {
                Id = p.Id,
                Name = p.Name,
                Address = p.Address,
                TelNumber = p.TelNumber
            };
            return ph;
        }
        public bool AddPublishingHouse(PublishingHouseItem p)
        {
            var ph = ConvertToDbItem(p);
            var newPH = repo.Add(ph);
            return true;
        }
        public PublishingHouseItem Update(PublishingHouseItem p)
        {
            var toDbPH = ConvertToDbItem(p);
            var pHret = repo.Update(toDbPH, toDbPH.Id);

            return ConvertToViewModel(pHret);
        }


        private BookForPHItem ConvertToViewModelwithBooks(Book b)
        {
            var result = new BookForPHItem
            {
                Id = b.Id,
                Name = b.Name,
                ImageThumbnailUrl = b.ImageThumbnailUrl,
                PublishingHouseId = b.PublishingHouseId
            };
            return result;
        }
        public PublishingHouseWithBooks ConvertPublishingHouseWithBooksToViewModel(PublishingHouse p)
        {
            List<BookForPHItem> ItemList = new List<BookForPHItem>();
            foreach (var item in p.Books)
            {
                ItemList.Add(ConvertToViewModelwithBooks(item));
            }
            var list = new PublishingHouseWithBooks
            {
                PublishingHouseId = p.Id,
                Name=p.Name,
                Books = ItemList
            };

            return list;
        }
        public List<PublishingHouseWithBooks> PublishingHousesWithBooks(int id)
        {
            var result = new List<PublishingHouseWithBooks>();
            var list = repo.GetPublishingHouseWithBooks().Where(x => x.Id == id);
            foreach (var item in list)
            {
                result.Add(ConvertPublishingHouseWithBooksToViewModel(item));
            }
            return result;
        }


        //for console
        public IEnumerable<SelectListItem> GetDropDownPublishingHouses()
        {
            //var x = new (Select(x => new SelectListItem()
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString()
            //}
            return repo.GetAll().Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            //var drp = new DrpDwnCategoryItem() { Selectlist = liste };
            //return drp;
        }
    }
}
