﻿using System;
using System.Collections.Generic;
using System.Text;
using CodeFx.Business.Interfaces;
using CodeFx.Data.Interfaces;
using CodeFx.Data.Entities;
using CodeFx.ViewModel.Cart;
using System.Linq;

namespace CodeFx.Business.Implements
{
    public class ShoppingCartBusiness : IShoppingCartBusiness
    {
        private readonly IShoppingCartRepository _shoppingCartRepository;

        public ShoppingCartBusiness(IShoppingCartRepository shoppingCartRepository)
        {
            _shoppingCartRepository = shoppingCartRepository;
        }

        public CartInfo AddShoppingCart(int customerId, int bookId, double unitPrice, int quantity = 1) {
            
            var items = new List<CartItem> {
                       new CartItem {
                           BookId = bookId,
                           Quantity = quantity,
                           UnitPrice = unitPrice
                       }
            };

            return AddShoppingCart(customerId, items);
        }

        public CartInfo AddShoppingCart(int customerId, List<CartItem> items)
        {
            ShoppingCart dbCart = new ShoppingCart
            {
                CustomerId = customerId,
                Status = Infrastructure.Enums.Status.Active,
                ShoppingCartItems = (from i in items
                                     select new ShoppingCartItem
                                     {
                                         BookId = i.BookId,
                                         Quantity = i.Quantity,
                                         UnitPrice = i.UnitPrice,
                                         Amount = (i.Quantity * i.UnitPrice)
                                     }).ToList()
            };

            dbCart = _shoppingCartRepository.AddShoppingCart(dbCart);

            var vmCart = new CartInfo
            {
                CustomerId = customerId,
                CartId = dbCart.Id,
                Status = Infrastructure.Enums.Status.Active,
                Items = items
            };

            return vmCart;
        }

        public CartInfo UpdateOrRemoveShoppingCart(CartInfo cart) {

            if(cart.CustomerId < 1 || cart.Items == null || !cart.Items.Any())
            {
                // Ne isin var burada ?
                return null;
            }

            var checkDbCart = _shoppingCartRepository.Get(cart.CartId);

            if (checkDbCart == null) { // db de cart bilgisi yok ise buraya gir
                if (cart.Items.Any() && cart.Status == Infrastructure.Enums.Status.Active)
                { // itemlar var ve aktif cart ise db ye tekrar ekle.
                    cart = AddShoppingCart(cart.CustomerId, cart.Items);
                    return cart;
                }
                else {
                    return null; // hic birsey yok ise simdilik null gonderiyorum sonra baska ne yapilabilir bakacagim. CD
                }
            }

            if (checkDbCart.CustomerId != cart.CustomerId) { // DB deki customerId ile sepetteki customerId farkli ise data degismis olabilir hic birsey yapma.
                return null; // hic birsey yok ise simdilik null gonderiyorum sonra baska ne yapilabilir bakacagim. CD
            }


            // gercek update'e ve silme islemine basliyoruz


            if (cart.Items == null || !cart.Items.Any() || cart.Items.Sum(i=> i.Quantity) == 0) { // beni sil
                _shoppingCartRepository.Delete(checkDbCart);
                return null;
            }

            checkDbCart.ShoppingCartItems = (from i in cart.Items
                                             select new ShoppingCartItem
                                             {
                                                 BookId = i.BookId,
                                                 Quantity = i.Quantity,
                                                 UnitPrice = i.UnitPrice,
                                                 Amount = (i.Quantity * i.UnitPrice)
                                             }).ToList();

            checkDbCart.ModifiedDateTime = DateTime.Now;
            checkDbCart.TotalAmount = checkDbCart.ShoppingCartItems.Sum(i => i.Amount);

            _shoppingCartRepository.UpdateShoppingCart(checkDbCart);

            return cart;
        }

        public CartInfo AddCartItem(CartInfo cart, int bookId, int quantity, double unitPrice)
        {
            var checkItem = cart.Items.FirstOrDefault(i => i.BookId == bookId);

            if (checkItem != null) // Yeni eklenen urun sepette var ise; ikinci kez eklemek yerine urun adetini arttirmamiz yeterli
            {                       // ikinci kez eklenen urunun fiyati bir oncekinden farkli ise yeni fiyati set ediyoruz. 
                                    // fiyati degistirme islemi is modeline bagli olarak degistirilebilir. Ornek olsun diye ekledim.
                checkItem.Quantity += quantity;
                checkItem.UnitPrice = checkItem.UnitPrice != unitPrice ? unitPrice : checkItem.UnitPrice; // fiyat kontrolunu yapiyorum
            }
            else
            {
                cart.Items.Add(new CartItem
                {
                    BookId = bookId,
                    Quantity = quantity,
                    UnitPrice = unitPrice,
                    ModifiedDateTime = DateTime.Now
                });
            }
            cart.ModifiedDateTime = DateTime.Now;
            cart.TotalAmount = cart.Items.Sum(i => i.Amount); // sepete her eklenen urunde sepetin toplam tutarini yeniden hesaplamak gerekiyor.


            return UpdateOrRemoveShoppingCart(cart);
            //return cart;
        }

        // eger quantity degeri 0 yada sepetteki quantity degeri ile ayni ise sepetten silecek.
        // eger quantity degeri 0 degilse ve sepetteki degerinden farkli ise sepetten SILMEYECEK, quantity miktari sepettekinden dusulecek. 
        // Yeni deger eger 0 olursa bu sefer sepetten SILINECEK.
        public CartInfo RemoveCartItem(CartInfo cart, int bookId, int quantity = 0)
        {
            var checkItem = cart.Items.SingleOrDefault(i => i.BookId == bookId);
            if (checkItem != null)
            {
                if (quantity == 0 || checkItem.Quantity == quantity)
                {
                    cart.Items.Remove(checkItem);
                }
                else
                {
                    checkItem.Quantity -= quantity;
                    checkItem.ModifiedDateTime = DateTime.Now;
                    if (checkItem.Quantity < 1)
                    { // olasi 0 sifirlama hatalarinin onune gecmek icin 1 den kucuk mu kontrolu yaptim.
                        cart.Items.Remove(checkItem);
                    }
                }
                cart.ModifiedDateTime = DateTime.Now;
            }

            cart.TotalAmount = cart.Items.Sum(i => i.Amount);

            return UpdateOrRemoveShoppingCart(cart);
            //return cart;
        }
    }
}