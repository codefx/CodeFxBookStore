﻿using CodeFx.Business.Implements;
using CodeFx.Business.Interfaces;
using CodeFx.Infrastructure.Security;
using Microsoft.Extensions.DependencyInjection;

namespace CodeFx.Business.Injector
{
    public static class DependencyInjection
    {
        public static void Init(IServiceCollection services) {
            services.AddScoped<IHashProvider, SaltedHash>();
            services.AddScoped<IBookBusiness, BookBusiness>();
            services.AddScoped<ICategoryBusiness, CategoryBusiness>();
            services.AddScoped<ICustomerBusiness, CustomerBusiness>();
            services.AddScoped<IOrderBusiness, OrderBusiness>();
            services.AddScoped<IOrderDetailBusiness, OrderDetailBusiness>();
            services.AddScoped<IShoppingCartBusiness, ShoppingCartBusiness>();
            services.AddScoped<IAccountBusiness, AccountBusiness>();
            services.AddScoped<IPublishingHouseBusiness, PublishingHouseBusiness>();
            services.AddScoped<IAuthorBusiness, AuthorBusiness>();
            services.AddScoped<IAccountBusiness,AccountBusiness>();
        }
    }
}