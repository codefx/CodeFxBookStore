﻿using CodeFx.UI.Core.Common;
using CodeFx.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface IAccountBusiness
    {
        ResultSet<Login> LoginUser(Login model);

        ResultSet<Login> LoginMember(Login model);
        void LogoutAccount();
    }
}