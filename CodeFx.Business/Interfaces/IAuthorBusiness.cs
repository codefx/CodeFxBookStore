﻿using CodeFx.ViewModel.Author;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface IAuthorBusiness
    {
        IEnumerable<AuthorItem> GetAuthors();
        AuthorItem GetById(int id);
        IEnumerable<SelectListItem> GetDropDownAuthors();
        bool AddAuthor(AuthorItem a);
        AuthorItem Update(AuthorItem a);
        List<AuthorWithBooks> AuthorsWithBooks(int id);

    }
}
