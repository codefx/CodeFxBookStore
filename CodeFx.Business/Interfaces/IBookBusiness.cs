﻿using CodeFx.Data.Entities;
using CodeFx.ViewModel.Book;
using System.Collections.Generic;

namespace CodeFx.Business.Interfaces
{
    public interface IBookBusiness
    {
        BookItem GetById(int id);
       // IEnumerable<BookItem> GetLatestBooks();
        IEnumerable<BookItem> GetBooksSearch(string q);
        IEnumerable<Book> GetBooksConsole();
        BookEdit GetBookEditById(int id);
        BookEdit GetBookEditForCreate();
        BookEdit Update(BookEdit b);
        bool AddBook(BookEdit b);
    }
}