﻿using CodeFx.ViewModel.Category;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface ICategoryBusiness
    {
        IEnumerable<CategoryItem> GetCategories();
        CategoryItem GetById(int id);
        //DrpDwnCategoryItem DrpCategories();
        IEnumerable<SelectListItem> GetDropDownCategories();
        bool AddCategory(CategoryItem c);
        CategoryItem Update(CategoryItem c);
        List<CategoriesWithBooks> CategoriesWithBooks(int id);
    }
}
