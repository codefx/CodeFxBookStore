﻿using CodeFx.ViewModel.Customer;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface ICustomerBusiness
    {
        CustomerItem GetById(int id);
        IEnumerable<CustomerItem> GetCustomers();
        bool AddNewCustomer(NewCustomerItem c);
        NewCustomerItem Update(NewCustomerItem c);
    }
}
