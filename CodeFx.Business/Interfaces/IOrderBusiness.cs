﻿using CodeFx.ViewModel.Order;
using CodeFx.ViewModel.OrderDetail;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface IOrderBusiness
    {
        IEnumerable<OrderItem> GetOrders(int id);
        IEnumerable<OrderItem> GetAllOrders();
        //OrderItem GetById(int id);
        IEnumerable<OrderDetailItem> GetOrderDetails(int id);
    }
}
