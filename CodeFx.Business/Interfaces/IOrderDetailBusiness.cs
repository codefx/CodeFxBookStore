﻿using CodeFx.ViewModel.OrderDetail;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface IOrderDetailBusiness
    {
        IEnumerable<OrderDetailItem> GetOrderDetails(int id);
    }
}
