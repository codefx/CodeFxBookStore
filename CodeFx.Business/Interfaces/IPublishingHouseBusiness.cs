﻿using CodeFx.ViewModel.PublishingHouse;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface IPublishingHouseBusiness
    {
        IEnumerable<SelectListItem> GetDropDownPublishingHouses();
        IEnumerable<PublishingHouseItem> GetPublishingHouses();
        PublishingHouseItem GetById(int id);
        bool AddPublishingHouse(PublishingHouseItem p);
        PublishingHouseItem Update(PublishingHouseItem p);
        List<PublishingHouseWithBooks> PublishingHousesWithBooks(int id);
    }
}
