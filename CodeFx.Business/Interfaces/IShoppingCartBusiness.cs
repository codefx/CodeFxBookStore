﻿using CodeFx.ViewModel.Cart;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Business.Interfaces
{
    public interface IShoppingCartBusiness
    {
        CartInfo AddShoppingCart(int customerId, int bookId, double unitPrice, int quantity = 1);

        CartInfo AddShoppingCart(int customerId, List<CartItem> items);

        CartInfo UpdateOrRemoveShoppingCart(CartInfo cart);

        CartInfo AddCartItem(CartInfo cart, int bookId, int quantity, double unitPrice);

        CartInfo RemoveCartItem(CartInfo cart, int bookId, int quantity = 0);
    }
}
