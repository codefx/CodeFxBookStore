﻿using Microsoft.AspNetCore.Builder;

namespace CodeFx.Console.App_Code
{
    public static class MvcRoutes
    {
        public static void Init(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
