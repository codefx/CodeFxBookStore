﻿using CodeFx.Business.Interfaces;
using CodeFx.UI.Core.Manager;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Console.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountBusiness _accountBusiness;
        private readonly ISessionManager _sessionManager;
        public AccountController(IAccountBusiness accountBusiness, ISessionManager sessionManager)
        {
            _accountBusiness = accountBusiness;
            _sessionManager = sessionManager;
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Login(ViewModel.Account.Login model) {

            var result = _accountBusiness.LoginUser(model);
            if (result.Success) {
                return RedirectToAction("Index", "Home");
            }
            return View(result);
        }

        public IActionResult Logout()
        {

            if (HttpContext.Session != null)
            {
                _accountBusiness.LogoutAccount();
                HttpContext.Session.Clear();
            }
            
            return RedirectToAction("Login");
        }
    }
}