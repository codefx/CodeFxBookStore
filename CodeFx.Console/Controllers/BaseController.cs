﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeFx.Infrastructure.Structs;
using CodeFx.UI.Core.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Console.Controllers
{
    [Security(Keys.Security.CommonScreens)]
    public class BaseController : Controller
    {
        
    }
}