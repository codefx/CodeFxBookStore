﻿using CodeFx.Business.Interfaces;
using CodeFx.Infrastructure.Structs;
using CodeFx.UI.Core.Attributes;
using CodeFx.ViewModel.Book;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Console.Controllers
{
   
    public class BookController : BaseController
    {
        private readonly IBookBusiness _bookBusiness;
        
        public BookController(IBookBusiness bookBusiness)
        {
            _bookBusiness = bookBusiness;
        }
        // GET: Book
        public ActionResult Index()
        {
            var model = _bookBusiness.GetBooksConsole();
            return View(model);
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        
        // GET: Book/Create
        public ActionResult Create()
        {
            //return View();
            var model = _bookBusiness.GetBookEditForCreate();
            return View(model);
        }

        // POST: Book/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookEdit b)
        {
            try
            {
                _bookBusiness.AddBook(b);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            var x = _bookBusiness.GetBookEditById(id);
            return View(x);
        }

        // POST: Book/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, BookEdit b)
        {
            try
            {
                _bookBusiness.Update(b);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Book/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}