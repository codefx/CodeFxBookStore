﻿using CodeFx.Business.Interfaces;
using CodeFx.ViewModel.Category;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Console.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryBusiness _categoryBusiness;

        public CategoryController(ICategoryBusiness categoryBusiness)
        {
            _categoryBusiness = categoryBusiness;
        }
        // GET: Category
        public ActionResult Index()
        {
            var list = _categoryBusiness.GetCategories();
            return View(list);
        }

       

        // GET: Category/Create
        public ActionResult AddCategory()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCategory(CategoryItem c)
        {
            try
            {
                _categoryBusiness.AddCategory(c);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult UpdateCategory(int id)
        {
            var x = _categoryBusiness.GetById(id);
            return View(x);
        }

        // POST: Category/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateCategory(CategoryItem c)
        {
            try
            {
                _categoryBusiness.Update(c);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Category/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        
    }
}