﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using CodeFx.Console.Models;
using CodeFx.UI.Core.Attributes;
using CodeFx.Infrastructure.Structs;
using CodeFx.UI.Core.Manager;
using CodeFx.UI.Core.Account;

namespace CodeFx.Console.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ISessionManager _sessionManager;
        public HomeController(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
