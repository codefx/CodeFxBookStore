﻿using CodeFx.Business.Interfaces;
using CodeFx.ViewModel.PublishingHouse;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Console.Controllers
{
    public class PublishingHouseController : BaseController
    {
        private readonly IPublishingHouseBusiness _publishingHouseBusiness;

        public PublishingHouseController(IPublishingHouseBusiness publishingHouseBusiness)
        {
            _publishingHouseBusiness = publishingHouseBusiness;
        }
        // GET: Category
        public ActionResult Index()
        {
            var list = _publishingHouseBusiness.GetPublishingHouses();
            return View(list);
        }

        // GET: PublishingHouse/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PublishingHouse/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PublishingHouse/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PublishingHouseItem p)
        {
            try
            {
                _publishingHouseBusiness.AddPublishingHouse(p);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PublishingHouse/Edit/5
        public ActionResult Edit(int id)
        {
            var x = _publishingHouseBusiness.GetById(id);
            return View(x);
        }

        // POST: PublishingHouse/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PublishingHouseItem p)
        {
            try
            {
                _publishingHouseBusiness.Update(p);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PublishingHouse/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PublishingHouse/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}