﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFx.Data.Entities
{
    public class Author
    {   [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public  virtual ICollection<Book> Books { get; set; }
        
    }
}
