﻿using CodeFx.Infrastructure.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFx.Data.Entities
{
    public class Book
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        public string Isbn { get; set; }

        public int AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public virtual  Author Author { get; set; }

        public int PublishingHouseId { get; set; }

        [ForeignKey("PublishingHouseId")]
        public virtual PublishingHouse PublishingHouse { get; set; } 

        public Language Language { get; set; }
        public string PageCount { get; set; }
        public string Edition { get; set; }
        public string Volume { get; set; }
        public string ImageUrl { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public string ShortDescription { get; set; }

        public decimal Price { get; set; }        
        public bool InStock { get; set; }
               
        public int? CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; } 
    }
}
