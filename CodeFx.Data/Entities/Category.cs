﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFx.Data.Entities
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}