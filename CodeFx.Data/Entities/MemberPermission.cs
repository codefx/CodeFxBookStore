﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Entities
{
    public class MemberPermission
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int PermissionId { get; set; }
        public bool IsAccessible { get; set; }
        
        public virtual Member Member { get; set; }
        public virtual Permission Permission { get; set; }
    }
}
