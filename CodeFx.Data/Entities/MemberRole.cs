﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Entities
{
   public  class MemberRole
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int RoleId { get; set; }

        public virtual Member Member { get; set; }
        public virtual Role Role { get; set; }
    }
}
