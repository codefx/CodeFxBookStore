﻿using System.Collections.Generic;

namespace CodeFx.Data.Entities
{
    public class Permission
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }

        public virtual ICollection<MemberPermission> MemberPermissions { get; set; }
        public virtual ICollection<RolePermission> RolePermissions { get; set; }
        public virtual ICollection<UserPermission> UserPermissions { get; set; }
    }
}
