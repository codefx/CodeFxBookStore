﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFx.Data.Entities
{
    public class PublishingHouse
    {   [Key]
        public int Id { get; set; }
        public string Name { get; set; }       
        public string Address { get; set; }
        public string TelNumber { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}
