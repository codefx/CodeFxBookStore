﻿using CodeFx.Infrastructure.Enums;
using System.Collections.Generic;

namespace CodeFx.Data.Entities
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public Status StatusId { get; set; }
        
        public virtual ICollection<MemberRole> MemberRoles { get; set; }
        public virtual ICollection<RolePermission> RolePermissions { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
