﻿using System;
using System.Collections.Generic;
using CodeFx.Infrastructure.Enums;

namespace CodeFx.Data.Entities
{
    public class ShoppingCart
    {
        public ShoppingCart()
        {
            ShoppingCartItems = new List<ShoppingCartItem>();
            Status = Status.Active;
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Status Status { get; set; }

        public virtual ICollection<ShoppingCartItem> ShoppingCartItems { get; set; }

        public double TotalAmount { get; set; }

        public DateTime ModifiedDateTime { get; set; }

        

    }
}
