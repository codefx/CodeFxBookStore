﻿using System;

namespace CodeFx.Data.Entities
{
    public class ShoppingCartItem
    {
        public int ShoppingCartItemId { get; set; }

        public int ShoppingCartId { get; set; }

        public DateTime AddedDateTime { get; private set; }

        public DateTime ModifiedDateTime { get; set; }

        //public Book Book { get; set; }

        public int BookId { get; set; }

        public int Quantity { get; set; }
        public double UnitPrice { get; set; }

        public double Amount { get; set; }

        public ShoppingCartItem()
        {
            AddedDateTime = DateTime.Now;
            ModifiedDateTime = DateTime.Now;
        }
    }
}