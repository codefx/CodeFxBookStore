﻿using CodeFx.Infrastructure.Enums;
using System;
using System.Collections.Generic;

namespace CodeFx.Data.Entities
{
    public class User
    {
        public int Id { get; set; }
        public int? ParentUserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public Status StatusId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual IEnumerable<UserRole> UserRoles { get; set; }        
        public virtual IEnumerable<UserPermission> UserPermissions { get; set; }
    }
}
