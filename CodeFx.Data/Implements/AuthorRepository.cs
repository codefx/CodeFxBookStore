﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CodeFx.Data.Implements
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(DataContext context) : base(context)
        {
        }
        public IEnumerable<Author> GetAuthorWithBooks()
        {
            var list = _context.Authors.Include(x => x.Books).ToList();
            return list;

        }
    }
}
