﻿using System;
using System.Collections.Generic;
using System.Text;
using CodeFx.Data.Interfaces;
using CodeFx.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CodeFx.Data.Implements
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        public BookRepository(DataContext context) : base(context)
        {
        }
        //public override Book Get(int id)
        //{
        //    var b = _context.Books.Include(x => x.Author)
        //                          .Include(x => x.Category)
        //                          .Include(x => x.PublishingHouse)
        //                          .FirstOrDefault(x => x.Id == id);
        //    return b;
        //}
    }
}
