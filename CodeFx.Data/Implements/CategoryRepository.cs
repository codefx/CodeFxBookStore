﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Data.Implements
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DataContext context) : base(context)
        {
        }
        public IEnumerable<Category> GetCategorieswithBooks()
        {
            var list = _context.Categories.Include(x => x.Books).ToList();
            return list;

        }
    }
}
