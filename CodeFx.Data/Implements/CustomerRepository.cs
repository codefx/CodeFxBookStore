﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Implements
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DataContext context) : base(context)
        {
        }
    }
}
