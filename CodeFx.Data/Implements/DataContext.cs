using CodeFx.Data.Entities;
using CodeFx.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CodeFx.Data.Implements
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {
            
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Author> Authors { get; set; }       
        public DbSet<PublishingHouse> PublishingHouses { get; set; }        
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        public DbSet<Customer> Customers { get; set; }


        public DbSet<User> Users { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<MemberRole> MemberRoles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<MemberPermission> MemberPermissions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MemberPermission>(entity =>
            {
                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MemberPermissions)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.MemberPermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MemberRole>(entity =>
            {
                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MemberRoles)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.MemberRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Member>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Description)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RolePermission>(entity =>
            {
                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Description)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<UserPermission>(entity =>
            {
                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.UserPermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserPermissions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(85)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(125)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Category>()
                  .HasMany(b => b.Books)
                  .WithOne(b=>b.Category)
                  .IsRequired();
            modelBuilder.Entity<Author>()
                  .HasMany(b => b.Books)
                  .WithOne(b => b.Author)
                  .IsRequired();
            modelBuilder.Entity<PublishingHouse>()
                  .HasMany(b => b.Books)
                  .WithOne(b => b.PublishingHouse)
                  .IsRequired();

            base.OnModelCreating(modelBuilder);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.Entity<Book>().Property(s => s.Language).HasDefaultValue(Language.Turkish);

            #region Seeding Demo data
            // id leri sak�n "" --> string girme,
            // enum lar� enum.key olarak gir,
            //decimal da m, double d yazmay� unutma !!!!
            // yani db de ki tam kar��l���n� yaz..--> entity class '�n dan

            /*********************************************
             login ekran�nda user=> codefx@codefx.com, password=>123456
             */ 
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "CodeFx", Surname = "Coder", Email = "codefx@codefx.com", PasswordHash = "eKRq0aKpeSqn97oRaqUnnjLiGrstYVkHy7hcow1jRGA=", PasswordSalt = "lTm3M4KO82Qry+pAKBB78w==",StatusId=Status.Active });
            modelBuilder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Admin", Description = "System Admin", TypeId = 1, StatusId = Status.Active });
            modelBuilder.Entity<UserRole>().HasData(
                new UserRole { Id = 1, UserId = 1, RoleId = 1 });

            modelBuilder.Entity<PublishingHouse>().HasData(

                new { Id = 1, Name = "Superior", Address = "abc-xyz 15", TelNumber = "12345" },
                new { Id = 2, Name = "Elegant", Address = "abc-xyz 15", TelNumber = "12345" },
                new { Id = 3, Name = "BookHouse", Address = "abc-xyz 15", TelNumber = "12345" }
            );
            /********************************/

            modelBuilder.Entity<Category>().HasData(
                new { Id = 1, Name = "Programming", Description = "All programming books" },
                new { Id = 2, Name = "Literature", Description = "All literature books" },
                new { Id = 3, Name = "Drawing", Description = "All drawing books" });
            /*********************************/

            modelBuilder.Entity<Author>().HasData(
              new { Id = 1, Name = "Adam", Surname = "Freeman" },
              new { Id = 2, Name = "Suzan", Surname = "Freeman" },
              new { Id = 3, Name = "Clark", Surname = "Kent" });
            /**************************************/
            // List<Book> books = new List<Book>() foreign key y�z�nden anonymous tip verildi.
            modelBuilder.Entity<Book>().HasData(
                new
                {
                    Id = 1,
                    AuthorId = 1,
                    Name = "C#",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "300",
                    Price = 30m,
                    Volume = "1",
                    PublishingHouseId = 1,
                    CategoryId = 1,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                },

                new
                {
                    Id = 2,
                    AuthorId = 2,
                    Name = "Earth",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "250",
                    Price = 20m,
                    Volume = "1",
                    PublishingHouseId = 2,
                    CategoryId = 2,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                },

                new
                {
                    Id = 3,
                    AuthorId = 3,
                    Name = "How to Draw?",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "300",
                    Price = 30m,
                    Volume = "1",
                    PublishingHouseId = 3,
                    CategoryId = 3,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                }, new
                {
                    Id = 4,
                    AuthorId = 1,
                    Name = "Python",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "300",
                    Price = 30m,
                    Volume = "1",
                    PublishingHouseId = 1,
                    CategoryId = 1,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                },

                new
                {
                    Id = 5,
                    AuthorId = 2,
                    Name = "Moon",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "250",
                    Price = 20m,
                    Volume = "1",
                    PublishingHouseId = 2,
                    CategoryId = 2,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                },

                new
                {
                    Id = 6,
                    AuthorId = 3,
                    Name = "How to Paint?",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "300",
                    Price = 30m,
                    Volume = "1",
                    PublishingHouseId = 3,
                    CategoryId = 3,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                }, new
                {
                    Id = 7,
                    AuthorId = 1,
                    Name = "Java",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "300",
                    Price = 30m,
                    Volume = "1",
                    PublishingHouseId = 1,
                    CategoryId = 1,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                },

                new
                {
                    Id = 8,
                    AuthorId = 2,
                    Name = "Mars",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "250",
                    Price = 20m,
                    Volume = "1",
                    PublishingHouseId = 2,
                    CategoryId = 2,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                },

                new
                {
                    Id = 9,
                    AuthorId = 3,
                    Name = "Painting",
                    Isbn = "1234567",
                    Edition = "1.",
                    PageCount = "300",
                    Price = 30m,
                    Volume = "1",
                    PublishingHouseId = 3,
                    CategoryId = 3,
                    InStock = true,
                    Language = Language.Turkish,
                    ImageUrl = "hdgjkfdhjdfhglkfdklfj",
                    ImageThumbnailUrl = "shdgfdsjdsho�fdhg�gj",
                    ShortDescription = "ugfkgkdkjdhldfhbldh"
                }
                );
            /*********************************/  
           

            #endregion
        }

    }
}