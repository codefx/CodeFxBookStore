﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeFx.Data.Implements
{
    public class MemberRepository : BaseRepository<Member>, IMemberRepository
    {
        public MemberRepository(DataContext context) : base(context)
        {
        }

        public Member GetMemberByEmail(string email)
        {
            return _context.Members.FirstOrDefault(x => x.Email.Equals(email));
        }
        public List<Permission> GetMemberRolePermission(Member member)
        {

            var result = new List<Permission>();
            foreach (var memberRole in member.MemberRoles)
            {
                foreach (var permission in memberRole.Role.RolePermissions)
                {
                    result.Add(permission.Permission);
                }
            }

            return result;
        }
    }
}
