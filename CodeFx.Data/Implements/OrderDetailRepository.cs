﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Implements
{
    public class OrderDetailRepository : BaseRepository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(DataContext context) : base(context)
        {
        }
    }
}
