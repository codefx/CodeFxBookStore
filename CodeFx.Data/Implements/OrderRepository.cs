﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Implements
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(DataContext context) : base(context)
        {
        }
    }
}
