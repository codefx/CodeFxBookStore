﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Data.Implements
{
    public class PublishingHouseRepository : BaseRepository<PublishingHouse>, IPublishingHouseRepository
    {
        public PublishingHouseRepository(DataContext context) : base(context)
        {
        }
        public IEnumerable<PublishingHouse> GetPublishingHouseWithBooks()
        {
            var list = _context.PublishingHouses.Include(x => x.Books).ToList();
            return list;

        }
    }
}
