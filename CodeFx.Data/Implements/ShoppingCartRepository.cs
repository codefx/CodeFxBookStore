﻿using System;
using System.Collections.Generic;
using System.Text;
using CodeFx.Data.Interfaces;
using CodeFx.Data.Entities;

namespace CodeFx.Data.Implements
{
    public class ShoppingCartRepository : BaseRepository<ShoppingCart>, IShoppingCartRepository
    {
        public ShoppingCartRepository(DataContext context) : base(context)
        {
        }


        public ShoppingCart AddShoppingCart(ShoppingCart newCart) {

            Add(newCart);

            return newCart;
        }

        public ShoppingCart UpdateShoppingCart(ShoppingCart updateCart) {

            _context.Update(updateCart);

            return updateCart;
        }

        public bool DeleteSoppingCart(ShoppingCart deleteSoppingCart) {

            Delete(deleteSoppingCart);

            return true;
        }
    }
}
