﻿using CodeFx.Data.Entities;
using CodeFx.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFx.Data.Implements
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DataContext context) : base(context)
        {
        }

        public User GetUserByEmail(string email)
        {
            return _context.Users.FirstOrDefault(x => x.Email.Equals(email));
        }
        public List<Permission> GetUserRolePermission(User user)
        {
            var result = new List<Permission>();
            foreach (var userRole in user.UserRoles) {
                foreach (var permission in userRole.Role.RolePermissions)
                {
                    result.Add(permission.Permission);
                }
            }

            return result;
        }
    }
}
