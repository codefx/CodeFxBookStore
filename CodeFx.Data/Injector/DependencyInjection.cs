﻿using CodeFx.Data.Implements;
using CodeFx.Data.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CodeFx.Data.Injector
{
    public static class DependencyInjection
    {
        public static void Init(IServiceCollection services) {

            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderDetailRepository, OrderDetailRepository>();
            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IPublishingHouseRepository, PublishingHouseRepository>();
            services.AddScoped<IShoppingCartRepository, ShoppingCartRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMemberRepository, MemberRepository>();
        }
    }
}