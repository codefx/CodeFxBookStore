﻿using System;
using System.Collections.Generic;
using System.Text;
using CodeFx.Data.Entities;
namespace CodeFx.Data.Interfaces
{
    public interface IBookRepository : IBaseRepository<Book>
    {
    }
}
