﻿using CodeFx.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Interfaces
{
    public interface IMemberRepository: IBaseRepository<Member>
    {
        List<Permission> GetMemberRolePermission(Member user);
        Member GetMemberByEmail(string email);
    }
}
