﻿using CodeFx.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Interfaces
{
    public interface IOrderDetailRepository: IBaseRepository<OrderDetail>
    {
    }
}
