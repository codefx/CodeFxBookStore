﻿using System;
using System.Collections.Generic;
using System.Text;
using CodeFx.Data.Interfaces;
using CodeFx.Data.Entities;

namespace CodeFx.Data.Interfaces
{
    public interface IShoppingCartRepository : IBaseRepository<ShoppingCart>
    {
        ShoppingCart AddShoppingCart(ShoppingCart newCart);

        ShoppingCart UpdateShoppingCart(ShoppingCart updateCart);

        bool DeleteSoppingCart(ShoppingCart deleteSoppingCart);
    }
}