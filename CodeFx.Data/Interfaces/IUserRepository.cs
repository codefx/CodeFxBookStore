﻿using CodeFx.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.Data.Interfaces
{
    public interface IUserRepository: IBaseRepository<User>
    {
        List<Permission> GetUserRolePermission(User user);
        User GetUserByEmail(string email);
    }

}
