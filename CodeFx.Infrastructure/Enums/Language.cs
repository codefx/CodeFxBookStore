﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CodeFx.Infrastructure.Enums
{
    public enum Language
    {
        [Description("Türkçe")]
        [Display(Name = "Turkish")]
        Turkish = 1,
        [Description("İngilizce")]
        [Display(Name = "English")]
        English = 2
    }
}
