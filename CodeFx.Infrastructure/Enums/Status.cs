﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CodeFx.Infrastructure.Enums
{
    public enum Status
    {
        [Description("Aktif")]
        [Display(Name = "Aktif")]
        Active = 1,

        [Description("Pasif")]
        [Display(Name = "Pasif")]
        Passive = 2,

        [Description("Silinmiş")]
        [Display(Name = "Silinmiş")]
        Deleted = 3,

        [Description("Satın Alındı")]
        [Display(Name = "Satın Alındı")]
        Purchased = 4,

        [Description("İptal Edilen")]
        [Display(Name = "İptal Edildi")]
        Cancelled = 5
    }
}
