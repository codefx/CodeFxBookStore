﻿namespace CodeFx.Infrastructure.Structs
{
    public struct Keys {
    
        public struct Security
        {
            public const string CommonScreens = "CommonScreens";
            public const string Admin = "Administrator";
        }

        public struct Session
        {
            public const string CurrentAccountInfo = "CurrentAccountInfo";
            public const string AfterLoginReturnUrl = "AfterLoginReturnUrl";
            public const string CurrentLanguageId = "CurrentLanguageId";
        }

        public struct Cookie {
            public const string Email = "email";
        }
    }
}