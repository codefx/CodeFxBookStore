﻿using CodeFx.Infrastructure.Enums;
using CodeFx.Infrastructure.Structs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeFx.UI.Core.Account
{
    [Serializable]
    public class AccountInfo
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }

        public int ParentId { get; set; }

        public string Email { get; set; }
        public bool LoginStatus { get; set; }
        public string Message { get; set; }
        public List<string> Rights { get; set; }

        public Status Status { get; set; }
        
        public bool HasPermissionFor(string objectName)
        {
            if (string.IsNullOrWhiteSpace(objectName))
                return false;

            if (objectName.Equals(Keys.Security.CommonScreens))
                return true;

            return !string.IsNullOrWhiteSpace(Rights.FirstOrDefault(r => r.Equals(objectName)));
        }

        public bool HasPermissionFor(string[] objectNames)
        {
            if (objectNames.Length == 0)
                return false;

            if (objectNames.Contains(Keys.Security.CommonScreens))
                return true;

            return !string.IsNullOrWhiteSpace(Rights.FirstOrDefault(r => objectNames.Contains(r)));
        }
    }
}