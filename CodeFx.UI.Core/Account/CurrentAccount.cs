﻿using CodeFx.Infrastructure.Structs;
using CodeFx.UI.Core.Manager;

namespace CodeFx.UI.Core.Account
{
    public class CurrentAccount
    {

        private ISessionManager _sessionManager;

        public CurrentAccount(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public AccountInfo Account {
            get {

                return _sessionManager.Get<AccountInfo>(Keys.Session.CurrentAccountInfo);
            }
            set {

                _sessionManager.Set(Keys.Session.CurrentAccountInfo, value);

            }
        }

        public bool IsAccountLogin
        {
            get { return Account != null && Account.LoginStatus; }
        }
    }
}
