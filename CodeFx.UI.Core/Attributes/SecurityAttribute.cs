﻿using CodeFx.UI.Core.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

using System;

namespace CodeFx.UI.Core.Attributes
{

    public class SecurityAttribute : ActionFilterAttribute
    {
        private CurrentAccount _currentAccount;

        public string[] Keys { get; private set; }

        public string ReturnUrl { get; private set; }

        public SecurityAttribute(string key = "", string returnUrl = "")
        {
           
            Keys = new string[] { (string.IsNullOrWhiteSpace(key) ? Infrastructure.Structs.Keys.Security.CommonScreens : key) };
            ReturnUrl = returnUrl;
        }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _currentAccount = filterContext.HttpContext.RequestServices.GetService<CurrentAccount>();

            if (_currentAccount == null || !_currentAccount.IsAccountLogin)
            {
                filterContext.Result = new RedirectResult("/Account/Login");
            }
            else
            {
                if (!_currentAccount.Account.HasPermissionFor(Keys)) { // yetkisi var mi kontrolunu yapiyoruz. varsa sıkıntı yok demek, devam etsın 
                    // eger yetkı yok ise logine yonlendırecegız
                    //filterContext.HttpContext.Response.Redirect("/Account/Login");
                    filterContext.Result = new RedirectResult("/Account/UnAuthorized");
                }
            }
        }

        //protected override void HandleUnauthorized(AuthorizationContext filterContext)
        //{

        //    var contextReturnUrl = "";
        //    if (_contextUserProviderBase.ContextBase.Request.Url != null)
        //    {
        //        contextReturnUrl = _contextUserProviderBase.ContextBase.Request.Url.ToString();
        //    }
        //    var retUrl = string.IsNullOrWhiteSpace(ReturnUrl) ? contextReturnUrl : ReturnUrl.Replace("~/", "/");
        //    _contextUserProviderBase.SessionManager.Set(Session.AfterLoginReturnUrl, retUrl);
        //    base.HandleUnauthorizedRequest(filterContext);
        //    filterContext.Result = new RedirectResult(PageUrl.AccountLogin);
        //}
    }
}
