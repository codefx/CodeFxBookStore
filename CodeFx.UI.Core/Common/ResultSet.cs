﻿using Newtonsoft.Json;
using System;

namespace CodeFx.UI.Core.Common
{
    /// <summary>
    /// Varsayılan değerleri 
    /// Success = false
    /// Message = "İşleminiz gerçekleştirilemedi"
    /// </summary>
    [JsonObject]
    [Serializable]
    public class ResultSet
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public void SetSuccessful(string message = "")
        {
            Success = true;
            Message = string.IsNullOrWhiteSpace(message) ? "" : message;
        }

        public ResultSet()
        {
            Success = false;
            Message = "";
        }

        public ResultSet(bool defaultValue)
        {
            Success = defaultValue;
            Message = defaultValue ? "İşlem başarılı." : "İşlem başarısız.";
        }
    }

    /// <summary>
    /// Varsayılan değerleri 
    /// Success = false
    /// Message = "İşleminiz gerçekleştirilemedi"
    /// </summary>
    /// 
    [JsonObject]
    [Serializable]
    public class ResultSet<T> : ResultSet
    {
        public T Object { get; set; }
    }
}
