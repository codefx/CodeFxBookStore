﻿using CodeFx.UI.Core.Manager;
using Microsoft.Extensions.DependencyInjection;

namespace CodeFx.UI.Core.Injector
{
    public static class DependencyInjection
    {
        public static void Init(IServiceCollection services)
        {
            services.AddSingleton<ISessionManager, SessionManager>();
            services.AddScoped<ICookieManager, CookieManager>();
        }
    }
}