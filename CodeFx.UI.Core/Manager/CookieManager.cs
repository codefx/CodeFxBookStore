﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace CodeFx.UI.Core.Manager
{
    public class CookieManager : ICookieManager
    {
        
        private IHttpContextAccessor _httpContext;

        public CookieManager(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }
        public string Name { get; set; }

        public int? ExpireMinute { get; set; }

        public bool IsHttpOnly { get; set; }

        /// <summary>
        /// Cookie'ye yazma işlemini yapar.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="name"></param>
        /// <param name="isHttpOnly"></param>
        /// <param name="expireMinute">Null set edilirse Session Bazlı set eder</param>
        /// <returns>true  / false </returns>
        public bool Write(object value, string name = "", bool isHttpOnly = true, int? expireMinute = null)
        {
            if (!string.IsNullOrWhiteSpace(name))
                Name = name;

            if (string.IsNullOrWhiteSpace(Name))
                return false;

            if (expireMinute.HasValue)
                ExpireMinute = expireMinute.Value;

            IsHttpOnly = isHttpOnly;

            var cookieOptions= new CookieOptions {
                HttpOnly = IsHttpOnly
            };

            if (ExpireMinute.HasValue)
            {
                cookieOptions.Expires = DateTime.Now.AddMinutes(ExpireMinute.Value);
            }

            _httpContext.HttpContext.Response.Cookies.Delete(Name);
            _httpContext.HttpContext.Response.Cookies.Append(Name, value.ToString(), cookieOptions);

            return true;
        }
        public string Read(string name = "")
        {
            if (!string.IsNullOrWhiteSpace(name))
                Name = name;

            if (string.IsNullOrWhiteSpace(Name))
                return null;
            

            var cookie = _httpContext.HttpContext.Request.Cookies.FirstOrDefault(k => k.Key.Equals(Name, StringComparison.OrdinalIgnoreCase));
            if (cookie.Key != null && cookie.Value != null) {
                return cookie.Value;
            }
            return "";
        }

        public bool Remove(string name = "")
        {
            if (!string.IsNullOrWhiteSpace(name))
                Name = name;

            if (string.IsNullOrWhiteSpace(Name))
                return false;

            var cookieOptions = new CookieOptions
            {
                HttpOnly = IsHttpOnly,
                Expires = DateTime.Now.AddDays(-1)
            };

            _httpContext.HttpContext.Response.Cookies.Delete(Name, cookieOptions);
            //_httpContext.HttpContext.Response.Cookies.Append(Name, "", cookieOptions);
            return true;
        }
    }
}
