﻿namespace CodeFx.UI.Core.Manager
{
    public interface ISessionManager
    {

        T Get<T>(string sessionName);

        void Set<T>(string sessionName, T obj);

        void Remove(string sessionName);
    }
}
