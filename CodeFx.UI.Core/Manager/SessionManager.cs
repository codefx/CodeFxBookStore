﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CodeFx.UI.Core.Manager
{
    public class SessionManager : ISessionManager
    {

        private IHttpContextAccessor _httpContext;

        public SessionManager(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        public T Get<T>(string sessionName)
        {
            var strVal = _httpContext.HttpContext.Session.GetString(sessionName);
            if (!string.IsNullOrWhiteSpace(strVal))
            {
                return JsonConvert.DeserializeObject<T>(strVal);
            }
            return default(T);
        }
        public void Remove(string sessionName)
        {
            _httpContext.HttpContext.Session.Remove(sessionName);
        }

        public void Set<T>(string sessionName,T obj)
        {
            _httpContext.HttpContext.Session.SetString(sessionName, JsonConvert.SerializeObject(obj));
        }

    }
}
