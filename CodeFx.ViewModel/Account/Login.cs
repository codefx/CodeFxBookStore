﻿using System.ComponentModel.DataAnnotations;

namespace CodeFx.ViewModel.Account
{
    public class Login
    {
        [Required(ErrorMessage = "Lütfen Email adres alanını doldurunuz..")]
        [EmailAddress(ErrorMessage = "Lütfen Geçerli bir email adresi giriniz..")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Lütfen şifre giriniz..")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
