﻿using System.ComponentModel.DataAnnotations;

namespace CodeFx.ViewModel.Account
{
    public class Register
    {
        [Required(ErrorMessage = "Lütfen Email adres alanını doldurunuz..")]
        [EmailAddress(ErrorMessage = "Lütfen Geçerli bir email adresi giriniz..")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Lütfen şifre giriniz..")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Lütfen şifreyi tekrar giriniz..")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Lütfen adınızı giriniz..")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Lütfen soyadınızı giriniz..")]
        public string Surname { get; set; }
    }
}