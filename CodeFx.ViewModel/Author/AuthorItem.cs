﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Author
{
    public class AuthorItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
