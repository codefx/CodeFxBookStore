﻿using CodeFx.ViewModel.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Author
{
    public class AuthorWithBooks
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }

        public List<BookForAuthorItem> Books { get; set; }
    }
}
