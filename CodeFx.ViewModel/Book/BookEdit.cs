﻿using CodeFx.Infrastructure.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace CodeFx.ViewModel.Book
{
    public class BookEdit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Isbn { get; set; }

        public int AuthorId { get; set; }

        public int PublishingHouseId { get; set; }

        public Language Language { get; set; }
        public string PageCount { get; set; }
        public string Edition { get; set; }
        public string Volume { get; set; }
        public string ImageUrl { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public string ShortDescription { get; set; }

        public decimal Price { get; set; }
        public bool InStock { get; set; }

        public int CategoryId { get; set; }

        public IEnumerable<SelectListItem> AuthorList;
        public IEnumerable<SelectListItem> PublishingHouseList;
        public IEnumerable<SelectListItem> CategoryList;       
    }
}
