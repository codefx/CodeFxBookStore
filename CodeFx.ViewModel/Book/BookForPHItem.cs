﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Book
{
    public class BookForPHItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public int PublishingHouseId { get; set; }
    }
}
