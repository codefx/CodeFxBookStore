﻿using CodeFx.Infrastructure.Enums;

using CodeFx.ViewModel.Category;

namespace CodeFx.ViewModel.Book
{
    public class BookItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Isbn { get; set; }
        public string Author { get; set; }
        public string PublishingHouse { get; set; }
        public Language Language { get; set; }
        public string PageCount { get; set; }
        public string Edition { get; set; }
        public string Volume { get; set; }
        public string ImageUrl { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public string ShortDescription { get; set; }
        public decimal Price { get; set; }
        public CategoryItem Category { get; set; }
    }
}
