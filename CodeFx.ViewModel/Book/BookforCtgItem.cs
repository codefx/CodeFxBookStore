﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Book
{
   public   class BookforCtgItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public int? CategoryId { get; set; }
    }
}
