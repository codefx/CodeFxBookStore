﻿using System;
using System.Collections.Generic;

namespace CodeFx.ViewModel.Cart
{
    public class CartInfo
    {
        public int CustomerId { get; set; }
        public int CartId { get; set; }
        public Infrastructure.Enums.Status Status { get; set; }
        public double TotalAmount { get; set; }
        public List<CartItem> Items { get; set; }
        public DateTime ModifiedDateTime { get; set; }

        public CartInfo()
        {
            Items = new List<CartItem>();
            ModifiedDateTime = DateTime.Now;
        }
    }
}