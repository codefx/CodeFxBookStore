﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Cart
{
    public class CartItem
    {
        public DateTime AddedDateTime { get; private set; }

        public DateTime ModifiedDateTime { get; set; }

        public int BookId { get; set; }

        public int Quantity { get; set; }
        public double UnitPrice { get; set; }

        public double Amount
        {
            get
            {
                return Quantity * UnitPrice;
            }
        }

        public CartItem()
        {
            AddedDateTime = DateTime.Now;
            ModifiedDateTime = DateTime.Now;
        }
    }
}
