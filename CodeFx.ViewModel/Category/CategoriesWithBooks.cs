﻿using CodeFx.ViewModel.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Category
{
    public class CategoriesWithBooks
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public List<BookforCtgItem> Books { get; set; }

    }
}
