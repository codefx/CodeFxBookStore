﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace CodeFx.ViewModel.Category
{
    public class CategoryItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public SelectList SelectList { get; }
    }
}