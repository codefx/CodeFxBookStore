﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Category
{
    public class DrpDwnCategoryItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SelectList Selectlist { get; set; }
    }
}
