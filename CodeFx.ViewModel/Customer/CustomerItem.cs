﻿using CodeFx.ViewModel.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Customer
{
    public class CustomerItem
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }
    }
}
