﻿using CodeFx.ViewModel.OrderDetail;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.Order
{
    public class OrderItem
    {
        public int Id { get; set; }

        public DateTime OrderDate { get; set; }

        public int CustomerId { get; set; }        

        public virtual IEnumerable<OrderDetailItem> OrderDetailItems { get; set; }

        public decimal OrderTotal { get; set; }
    }
}
