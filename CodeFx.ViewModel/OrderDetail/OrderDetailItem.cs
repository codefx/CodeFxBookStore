﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.OrderDetail
{
   public class OrderDetailItem
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int BookId { get; set; }
        public string BookName { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Amount { get; set; }
    }
}
