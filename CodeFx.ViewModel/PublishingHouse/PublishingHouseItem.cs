﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.PublishingHouse
{
    public class PublishingHouseItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string TelNumber { get; set; }
    }
}
