﻿using CodeFx.ViewModel.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFx.ViewModel.PublishingHouse
{
    public class PublishingHouseWithBooks
    {
        public int PublishingHouseId { get; set; }

        public string Name { get; set; }

        public List<BookForPHItem> Books { get; set; }
    }
}
