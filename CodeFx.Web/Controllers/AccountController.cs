﻿using Microsoft.AspNetCore.Mvc;
using CodeFx.ViewModel.Account;
using CodeFx.Business.Interfaces;
using CodeFx.UI.Core.Manager;
using CodeFx.Infrastructure.Structs;

namespace CodeFx.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountBusiness _accountBusiness;
        private readonly ISessionManager _sessionManager;
        private readonly ICookieManager _cookieManager;

        public AccountController(IAccountBusiness accountBusiness, ISessionManager sessionManager, ICookieManager cookieManager)
        {
            _accountBusiness = accountBusiness;
            _sessionManager = sessionManager;
            _cookieManager = cookieManager;
        }

        public IActionResult Login()
        {
            var model = new Login();

            var usernameCookie = _cookieManager.Read(Keys.Cookie.Email);
            if (!string.IsNullOrWhiteSpace(usernameCookie)) {
                model.RememberMe = true;
                model.Email = usernameCookie;
            }

            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Login(Login model)
        {
            if (ModelState.IsValid)
            {
                var result = _accountBusiness.LoginMember(model);
                if (result.Success)
                {
                    if (model.RememberMe) {
                        _cookieManager.Write(model.Email, Keys.Cookie.Email, true, int.MaxValue);
                    }

                    return Redirect("/");
                }
                ViewBag.ErrorMessage = result.Message;
            }

            return View(model);
        }

        public IActionResult LogOut()
        {
            _accountBusiness.LogoutAccount();
            HttpContext.Session.Clear();

            return Redirect("/");
        }

        public IActionResult Register() {

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Register(Register model)
        {
            if (ModelState.IsValid) {
                // Model ok ise bussiness a git

            }
            return View();
        }
    }
}