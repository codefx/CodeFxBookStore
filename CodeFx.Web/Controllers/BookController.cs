﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeFx.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Web.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookBusiness _bookBusiness;

        public BookController(IBookBusiness bookBusiness)
        {
            _bookBusiness = bookBusiness;
        }


        public IActionResult Index(string q)
        {
            var List = _bookBusiness.GetBooksSearch(q);
            return View(List);
        }
        //public PartialViewResult BookSearch(string q)
        //{
        //    var list = _bookBusiness.GetBooksSearch(q);
        //    return PartialView(list);
        //}

        public IActionResult Detail(int id)
        {
            if (id == 0) {
                return RedirectToAction("Index");
            }

            var book =  _bookBusiness.GetById(id);

            if (book == null) {
                return RedirectToAction("Index");
            }

            return View(book);
        }

        //public IActionResult Create()
        //{
            
        //    return View();
        //}
        //[HttpPost]
        //public IActionResult Create()
        //{

        //    return View();
        //}



    }
}