﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeFx.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodeFx.Web.Controllers
{
    public class PublishingHouseController : Controller
    {

        private readonly IPublishingHouseBusiness _publishingHouseBusiness;

        public PublishingHouseController(IPublishingHouseBusiness publishingHouseBusiness)
        {
            _publishingHouseBusiness = publishingHouseBusiness;
        }
       
        // GET: PublishingHouse
        public ActionResult Index()
        {
            var list = _publishingHouseBusiness.GetPublishingHouses();
            return View(list);
        }

        // GET: PublishingHouse/Details/5
        public ActionResult Details(int id)
        {
            var pHWB =_publishingHouseBusiness.PublishingHousesWithBooks(id);
            return View(pHWB);
        }

        // GET: PublishingHouse/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PublishingHouse/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PublishingHouse/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PublishingHouse/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PublishingHouse/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PublishingHouse/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}