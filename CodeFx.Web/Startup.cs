﻿using CodeFx.Data.Implements;
using CodeFx.UI.Core.Account;
using CodeFx.Web.App_Code;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CodeFx.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<DataContext>(options =>
                options.UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("CodeFxCon")));
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                //                options.Cookie.HttpOnly = true;
                options.IdleTimeout = TimeSpan.FromMinutes(40);
            });
            services.AddTransient<CurrentAccount>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            Data.Injector.DependencyInjection.Init(services);
            Business.Injector.DependencyInjection.Init(services);
            UI.Core.Injector.DependencyInjection.Init(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();

            MvcRoutes.Init(app);
        }
    }
}